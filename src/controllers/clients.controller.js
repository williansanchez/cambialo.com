const clientsCtrl = {};

// Models
const Client = require("../models/Client");

clientsCtrl.renderClientForm = (req, res) => {
  res.render("clients/new-client");
};

clientsCtrl.createNewClient = async (req, res) => {
  const { title, description } = req.body;
  const errors = [];
  if (!title) {
    errors.push({ text: "Por favor escriba un título." });
  }
  if (!description) {
    errors.push({ text: "Por favor escriba una descripción" });
  }
  if (errors.length > 0) {
    res.render("clients/new-client", {
      errors,
      title,
      description
    });
  } else {
    const newClient = new Client({ title, description });
    newClient.user = req.user.id;
    await newClient.save();
    req.flash("success_msg", "Cliente agregado correctamente.");
    res.redirect("/clients");
  }
};

clientsCtrl.renderClients = async (req, res) => {
  const clients = await Client.find({ user: req.user.id }).sort({ date: "desc" });
  res.render("clients/all-clients", { clients });
};

clientsCtrl.renderEditForm = async (req, res) => {
  const client = await Client.findById(req.params.id);
  if (client.user != req.user.id) {
    req.flash("error_msg", "No autorizado");
    return res.redirect("/clients");
  }
  res.render("clients/edit-client", { client });
};

clientsCtrl.updateClient = async (req, res) => {
  const { title, description } = req.body;
  await Client.findByIdAndUpdate(req.params.id, { title, description });
  req.flash("success_msg", "Cliente actualizado correctamente");
  res.redirect("/clients");
};

clientsCtrl.deleteClient = async (req, res) => {
  await Client.findByIdAndDelete(req.params.id);
  req.flash("success_msg", "Cliente eliminado correctamente");
  res.redirect("/clients");
};

module.exports = clientsCtrl;
