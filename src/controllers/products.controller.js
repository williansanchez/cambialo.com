const productsCtrl = {};

// Models
const Product = require("../models/Product");
const nodemailer = require('nodemailer');

productsCtrl.renderProductForm = (req, res) => {
  res.render("products/new-product");
};

productsCtrl.createNewProduct = async (req, res) => {
  const { title, description } = req.body;
  const errors = [];
  if (!title) {
    errors.push({ text: "Por favor escriba un título." });
  }
  if (!description) {
    errors.push({ text: "Por favor escriba una descripción" });
  }
  if (errors.length > 0) {
    res.render("products/new-product", {
      errors,
      title,
      description
    });
  } else {
    const newProduct = new Product({ title, description });
    newProduct.user = req.user.id;
    await newProduct.save();
    req.flash("success_msg", "Producto agregado correctamente.");
    res.redirect("/products");
  }
};

productsCtrl.renderProducts = async (req, res) => {
  const products = await Product.find({ user: req.user.id }).sort({ date: "desc" });
  res.render("products/all-products", { products });
};

productsCtrl.renderProductsAll = async (req, res) => {
  const products = await Product.find();
  res.render("products/products", { products });
};

productsCtrl.renderEditForm = async (req, res) => {
  const product = await Product.findById(req.params.id);
  if (product.user != req.user.id) {
    req.flash("error_msg", "No autorizado");
    return res.redirect("/products");
  }
  res.render("products/edit-product", { product });
};

productsCtrl.updateProduct = async (req, res) => {
  const { title, description } = req.body;
  await Product.findByIdAndUpdate(req.params.id, { title, description });
  req.flash("success_msg", "Producto actualizado correctamente");
  res.redirect("/products");
};

productsCtrl.deleteProduct = async (req, res) => {
  await Product.findByIdAndDelete(req.params.id);
  req.flash("success_msg", "Producto eliminado correctamente");
  res.redirect("/products");
};

productsCtrl.changeProduct = async (req, res) => {
  const product = await Product.findById(req.params.id);
  req.flash("success_msg", "Producto solicitado correctamente, pronto sera contactado");
  res.redirect("/productsAll");
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'williandersonsanchez@gmail.com',
      pass: 'Wil 1996'
    },
    tls: {
      rejectUnauthorized: false
    }
  });
    contentHTML = `
     <h1>Solicitud de cambio</h1>
     <li>Producto  ${product.title}</li>
     <li>Cliente Test Client</li>
`;
  const info = await transporter.sendMail({
    from: "'Servidor'",
    to: 'williandersonsanchez@gmail.com',
    subject: 'Notificacion de cambialo.com',
    html: contentHTML
  })
};

module.exports = productsCtrl;
