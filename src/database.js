const mongoose = require('mongoose')

const conn = async() => {

    try{
        await mongoose.connect( process.env.DATABASE || 'mongodb+srv://test:test@cluster0-kfpan.mongodb.net/test?retryWrites=true&w=majority',{
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useFindAndModify: true
        })
        console.log('database is connected')
    }
    catch(e){
        console.log(e)
    }
}

conn()