const express = require("express");
const router = express.Router();

// Controller
const {
  renderProductForm,
  createNewProduct,
  renderProducts,
  renderProductsAll,
  renderEditForm,
  updateProduct,
  deleteProduct,
  changeProduct
} = require("../controllers/products.controller");

// Helpers
const { isAuthenticated } = require("../helpers/auth");

// New products
router.get("/products/add", isAuthenticated, renderProductForm);

router.post("/products/new-product", isAuthenticated, createNewProduct);

// Get All products
router.get("/products", isAuthenticated, renderProducts);
router.get("/productsAll", renderProductsAll);
// Edit products
router.get("/products/edit/:id", isAuthenticated, renderEditForm);

router.put("/products/edit-product/:id", isAuthenticated, updateProduct);

// Delete products
router.delete("/products/delete/:id", isAuthenticated, deleteProduct);

// Change products
router.post("/products/change/:id", isAuthenticated, changeProduct);

module.exports = router;
