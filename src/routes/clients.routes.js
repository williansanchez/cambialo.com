const express = require("express");
const router = express.Router();

// Controller
const {
  renderClientForm,
  createNewClient,
  renderClients,
  renderEditForm,
  updateClient,
  deleteClient
} = require("../controllers/clients.controller");

// Helpers
const { isAuthenticated } = require("../helpers/auth");

// New Client
router.get("/clients/add", isAuthenticated, renderClientForm);

router.post("/clients/new-client", isAuthenticated, createNewClient);

// Get All Clients
router.get("/clients", isAuthenticated, renderClients);

// Edit Clients
router.get("/clients/edit/:id", isAuthenticated, renderEditForm);

router.put("/clients/edit-client/:id", isAuthenticated, updateClient);

// Delete Clients
router.delete("/clients/delete/:id", isAuthenticated, deleteClient);

module.exports = router;
